package nl.imreboersma.api;

import nl.imreboersma.api.endpoint.Tracks;
import nl.imreboersma.dao.*;
import nl.imreboersma.domain.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TracksTest {
  private static Tracks tracks;
  private ITrackListDAO iTrackListDAO;
  private IUserDAO iUserDAO;
  private IPlaylistDAO iPlaylistDAO;
  private ITrackDAO iTrackDAO;
  private IPlaylistListDAO iPlaylistListDAO;
  private User user;
  private Token token;
  private Song song;
  private TrackList trackList;

  @BeforeAll
  public static void setup() {
    tracks = new Tracks();
  }

  @BeforeEach
  public void mocking() {
    // Initialize variables
    user = new User();
    token = new Token();
    song = new Song();
    trackList = new TrackList();
    ArrayList<Track> trackArrayList = new ArrayList<>();

    // Mocking Song
    song.setPerformer("Bökkers");
    song.setTitle("Leaven In De Brouweri-je");
    song.setDuration(203);
    song.setUrl("https://open.spotify.com/track/0OtSEJvW7LYPOdGOKuwrwa");
    song.setAlbum("Leaven In De Brouweri-je");
    trackArrayList.add(song);

    // Mocking Tracklist
    trackList.setTracks(trackArrayList);

    // Mocking DAO's
    iTrackListDAO = mock(ITrackListDAO.class);
    iUserDAO = mock(IUserDAO.class);
    iPlaylistDAO = mock(IPlaylistDAO.class);

    // Injecting DAO's
    tracks.setIPlaylistDAO(iPlaylistDAO);
    tracks.setiUserDAO(iUserDAO);
    tracks.setiTrackListDAO(iTrackListDAO);
  }

  @Test
  public void getTracksFromPlaylistTest() {
    // Instructing mocks
    when(iPlaylistDAO.getTracksFromPlaylist(1)).thenReturn(trackList);

    // Testing
    Response response = tracks.getTracksFromPlaylist(token.getToken(), 1);

    // Assertion
    assertEquals(200, response.getStatus());
    assertEquals(trackList, response.getEntity());
  }

  @Test
  public void getTracksFromPlaylistTokenNullTest() {
  }

  @Test
  public void postTrackTest() {
  }

  @Test
  public void postTrackTrackNullTest() {

  }

  @Test
  public void postTrackTokenNullTest() {
  }

  @Test
  public void deleteTrackFromPlaylistTest() {
  }

  @Test
  public void deleteTrackFromPlaylistTokenNullTest() {
  }

  @Test
  public void deleteTrackFromPlaylistPlaylistIdNullTest() {
  }

}
