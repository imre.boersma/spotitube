package nl.imreboersma.api;

import nl.imreboersma.api.dto.TokenDTO;
import nl.imreboersma.dao.ITrackListDAO;
import nl.imreboersma.dao.IUserDAO;
import nl.imreboersma.domain.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class SpotitubeTest {

  private static Spotitube spotitube;
  private ITrackListDAO iTrackListDAO;
  private IUserDAO iUserDAO;
  private User user;
  private Token token;

  @BeforeAll
  public static void setup() {
    spotitube = new Spotitube();
  }

  @BeforeEach
  public void mocking() {
    // Initialize variables
    Song song = new Song();
    user = new User();
    token = new Token();
    TrackList trackList = new TrackList();
    ArrayList<Track> trackArrayList = new ArrayList<>();

    // Mocking DAO's
    iTrackListDAO = mock(ITrackListDAO.class);
    iUserDAO = mock(IUserDAO.class);

    // Mocking song
    song.setPerformer("Bökkers");
    song.setTitle("Leaven In De Brouweri-je");
    song.setDuration(203);
    song.setUrl("https://open.spotify.com/track/0OtSEJvW7LYPOdGOKuwrwa");
    song.setAlbum("Leaven In De Brouweri-je");
    trackArrayList.add(song);

    // Mocking User
    user.setUser("imreb");
    user.setPassword("password");

    // Mocking Token
    token.setToken("*5FF364A289F9770C8EF6143BE5060403EE7B283F");
    token.setUser("imreb");

    // Mocking Tracklist
    trackList.setTracks(trackArrayList);

    // Injecting DAO's
    spotitube.setIUserDAO(iUserDAO);
    spotitube.setITrackListDAO(iTrackListDAO);
  }

  @Test
  public void postLoginTest() {
    // Instructing mocks
    when(iUserDAO.login(user.getUser(), user.getPassword())).thenReturn(token);

    // Testing
    Response response = spotitube.postLogin(user);
    Token token1 = (Token) response.getEntity();

    // Assertion
    assertEquals(200, response.getStatus());
    assertEquals("imreb", token1.getUser());
    assertEquals(token.getToken(), token1.getToken());
  }

  @Test
  public void postLoginUnauthorizedTest() {
    // Instructing mocks
    when(iUserDAO.login(user.getUser(), user.getPassword())).thenReturn(null);

    // Testing
    Response response = spotitube.postLogin(user);
    TokenDTO tokenDTO = (TokenDTO) response.getEntity();

    // Assertion
    assertEquals(401, response.getStatus());
    assertNull(tokenDTO);
  }

  @Test
  public void getTracksTokenNullTest() {
    // Instructing mocks
    when(iTrackListDAO.getTracks(null, 1)).thenReturn(null);

    // Testing
    Response response = spotitube.getTracks(null, 1);
    Song actualTrack = (Song) response.getEntity();

    // Assertion
    assertEquals(403, response.getStatus());
    assertNull(actualTrack);
  }

  @Test
  public void getTracksForbiddenTest() {
    // Testing
    Response response = spotitube.getTracks(token.getToken(), 1);

    // Assertion
    assertEquals(403, response.getStatus());
    assertNull(response.getEntity());
  }

  @Test
  public void getTracksTest() {
    // Instructing mocks
    when(iUserDAO.getUser(token.getToken())).thenReturn("imreb");

    // Testing
    Response response = spotitube.getTracks(token.getToken(), 1);

    // Assertion
    assertEquals(200, response.getStatus());
  }
}
