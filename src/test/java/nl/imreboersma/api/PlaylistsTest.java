package nl.imreboersma.api;

import nl.imreboersma.api.endpoint.Playlists;
import nl.imreboersma.dao.*;
import nl.imreboersma.domain.Playlist;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PlaylistsTest {
  private static Spotitube spotitube;
  private static PlaylistListDAO playlistListDAO;
  private static Playlists playlists;
  private static IUserDAO iUserDAO;
  private String token = "*5FF364A289F9770C8EF6143BE5060403EE7B283F";

  @BeforeAll
  public static void setup() {
    spotitube = new Spotitube();
    playlistListDAO = new PlaylistListDAO();

    playlists = new Playlists();

    playlists.setIPlaylistDAO(mock(IPlaylistDAO.class));
    playlists.setIPlaylistListDAO(mock(IPlaylistListDAO.class));
    playlists.setIUserDAO(mock(IUserDAO.class));
    iUserDAO = mock(UserDAO.class);
    spotitube.setIUserDAO(iUserDAO);

  }

  @Test
  public void getPlaylistsTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    when(iUserDAO.getUser(token)).thenReturn("imreb");

    // Actual test
    Response response = playlists.getPlaylists(token);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void getPlaylistsTokenNullTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.getPlaylists(null);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void getPlaylistsNotFoundTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.getPlaylists(null);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void postPlaylistTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.postPlaylist(playlist, token);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void postPlaylistTokenNullTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.postPlaylist(playlist, null);

    // Assertion
    assertEquals(403, response.getStatus());

  }

  @Test
  public void deletePlaylistTest() {
  }

  @Test
  public void deletePlaylistTokenNullTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.deletePlaylist(1, null);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void putPlaylistTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imreb");
    when(iUserDAO.getUser(token)).thenReturn("imreb");

    // Actual test
    Response response = playlists.putPlaylist(playlist, 1, null);

    // Assertion
    assertEquals(403, response.getStatus());
  }

  @Test
  public void putPlaylistTokenNullTest() {
    Playlist playlist = new Playlist();
    playlist.setId(1);
    playlist.setName("test Playlist");
    playlist.setOwner(false);
    playlist.setOwnerName("imrea");

    // Actual test
    Response response = playlists.putPlaylist(playlist, 1, null);

    // Assertion
    assertEquals(403, response.getStatus());
  }
}
