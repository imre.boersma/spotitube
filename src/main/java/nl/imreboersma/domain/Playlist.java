package nl.imreboersma.domain;

import nl.imreboersma.dao.ITrackDAO;

import java.util.ArrayList;

public class Playlist {
  public int id;
  public String name;
  public String ownerName;
  public Boolean owner;
  public ArrayList<Track> tracks;
  ITrackDAO iTrackDAO;

  public Playlist(int id, String name, String ownerName, Boolean owner) {
    this.id = id;
    this.name = name;
    this.ownerName = ownerName;
    this.owner = owner;
  }

  public Playlist() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public Boolean getOwner() {
    return owner;
  }

  public void setOwner(Boolean owner) {
    this.owner = owner;
  }

  public ArrayList<Track> getTracks() {
    return tracks;
  }

}
