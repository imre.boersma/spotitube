package nl.imreboersma.domain;

public class Song extends Track {
  public String album;

  public String getAlbum() {
    return album;
  }

  public void setAlbum(String album) {
    this.album = album;
  }
}
