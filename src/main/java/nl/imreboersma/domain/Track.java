package nl.imreboersma.domain;

public class Track {
  public int id;
  public String performer;
  public String title;
  public String url;
  public Integer duration;
  public Boolean offlineAvailable;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getPerformer() {
    return performer;
  }

  public void setPerformer(String performer) {
    this.performer = performer;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getOfflineAvailable() {
    return offlineAvailable;
  }

  public void setOfflineAvailable(Boolean offlineAvailable) {
    this.offlineAvailable = offlineAvailable;
  }

  public Integer getDuration() {
    return this.duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }
}
