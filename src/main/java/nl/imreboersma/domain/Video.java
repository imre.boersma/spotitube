package nl.imreboersma.domain;

import java.util.Date;

public class Video extends Track {
  public Date publicationDate;
  public String description;

  public Date getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(Date publicationDate) {
    this.publicationDate = publicationDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
