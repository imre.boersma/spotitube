package nl.imreboersma.domain;

import java.util.ArrayList;

public class TrackList {
  public ArrayList<Track> tracks;

  public ArrayList<Track> getTracks() {
    return tracks;
  }

  public void setTracks(ArrayList<Track> tracks) {
    this.tracks = tracks;
  }
}
