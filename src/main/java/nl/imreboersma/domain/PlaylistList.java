package nl.imreboersma.domain;

import java.util.ArrayList;

public class PlaylistList {
  public ArrayList<Playlist> playlists;
  public int length;

  public ArrayList<Playlist> getPlaylists() {
    return playlists;
  }

  public void setPlaylists(ArrayList<Playlist> playlists) {
    this.playlists = playlists;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }
}
