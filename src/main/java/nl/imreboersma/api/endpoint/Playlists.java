package nl.imreboersma.api.endpoint;

import nl.imreboersma.dao.IPlaylistDAO;
import nl.imreboersma.dao.IPlaylistListDAO;
import nl.imreboersma.dao.IUserDAO;
import nl.imreboersma.domain.Playlist;
import nl.imreboersma.domain.PlaylistList;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/playlists")
public class Playlists {
  private IPlaylistListDAO iPlaylistListDAO;
  private IPlaylistDAO iplaylistDAO;
  private IUserDAO iUserDAO;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getPlaylists(@QueryParam("token") String token) {
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    PlaylistList playlistList = new PlaylistList();
    playlistList.setPlaylists(iPlaylistListDAO.getPlaylists(token));
    playlistList.setLength(iPlaylistListDAO.calculateLength(iPlaylistListDAO.getPlaylists(token)));
    if(playlistList.getPlaylists() == null) {
      return Response.status(404).build();
    }
    return Response.status(200).entity(playlistList).build();
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public Response postPlaylist(@NotNull Playlist playlist, @QueryParam("token") String token) {
    String string = iUserDAO.getUser(token);
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    return Response.status(201).entity(iplaylistDAO.postPlaylist(playlist, token)).build();
  }

  @DELETE
  @Path("/{id}")
  public Response deletePlaylist(@PathParam("id") Integer id, @QueryParam("token") String token) {
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    return Response.status(200).entity(iplaylistDAO.deletePlaylist(id, token)).build();
  }

  @PUT
  @Path("/{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response putPlaylist(@NotNull Playlist playlist, @PathParam("id") Integer id, @QueryParam("token") String token) {
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    return Response.status(200).entity(iplaylistDAO.putPlaylist(playlist, id, token)).build();
  }

  @Inject
  public void setIPlaylistListDAO(IPlaylistListDAO iPlaylistListDAO) {
    this.iPlaylistListDAO = iPlaylistListDAO;
  }

  @Inject
  public void setIPlaylistDAO(IPlaylistDAO iplaylistDAO) {
    this.iplaylistDAO = iplaylistDAO;
  }

  @Inject
  public void setIUserDAO(IUserDAO iUserDAO) {
    this.iUserDAO = iUserDAO;
  }
}
