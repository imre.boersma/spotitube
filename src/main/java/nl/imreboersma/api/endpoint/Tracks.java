package nl.imreboersma.api.endpoint;

import nl.imreboersma.dao.IPlaylistDAO;
import nl.imreboersma.dao.ITrackListDAO;
import nl.imreboersma.dao.IUserDAO;
import nl.imreboersma.domain.Track;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("playlists/{id}/tracks")
public class Tracks {

  private IPlaylistDAO iplaylistDAO;
  private IUserDAO iUserDAO;
  private ITrackListDAO iTrackListDAO;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getTracksFromPlaylist(@QueryParam("token") String token, @PathParam("id") int id) {
    return Response.status(200).entity(iplaylistDAO.getTracksFromPlaylist(id)).build();
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response postTrack(@NotNull Track track, @QueryParam("token") String token, @PathParam("id") int id) {
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    iplaylistDAO.addTrackToPlaylist(track, id, token);
    return Response.status(201).build();
  }

  @DELETE
  @Path("/{trackId}")
  public Response deleteTrackFromPlaylist(@PathParam("id") int playlistId, @PathParam("trackId") int trackId, @QueryParam("token") String token) {
    if(iUserDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    if(playlistId <= 0) {
      return Response.status(404).build();
    }
    iplaylistDAO.deleteTrackFromPlaylist(playlistId, trackId, token);
    return Response.status(200).build();
  }

  @Inject
  public void setIPlaylistDAO(IPlaylistDAO iplaylistDAO) {
    this.iplaylistDAO = iplaylistDAO;
  }

  @Inject
  public void setiUserDAO(IUserDAO iUserDAO) {
    this.iUserDAO = iUserDAO;
  }

  @Inject
  public void setiTrackListDAO(ITrackListDAO iTrackListDAO) {
    this.iTrackListDAO = iTrackListDAO;
  }
}
