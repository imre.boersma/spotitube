package nl.imreboersma.api.dto;

import nl.imreboersma.domain.Playlist;

import java.util.ArrayList;

public class PlaylistListDTO {
  public ArrayList<Playlist> playlists;
  public int length;
}
