package nl.imreboersma.api.dto;

import nl.imreboersma.domain.Track;

import java.util.ArrayList;

public class TrackListDTO {
  public ArrayList<Track> tracks;
}
