package nl.imreboersma.api.dto;

public class TrackDTO {
  public int id;
  public String performer;
  public String title;
  public String url;
  public Integer duration;
  public Boolean offlineAvailable;
}
