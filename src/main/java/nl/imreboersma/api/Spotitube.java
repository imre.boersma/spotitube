package nl.imreboersma.api;

import nl.imreboersma.dao.ITrackListDAO;
import nl.imreboersma.dao.IUserDAO;
import nl.imreboersma.dao.TrackListDAO;
import nl.imreboersma.dao.UserDAO;
import nl.imreboersma.domain.Token;
import nl.imreboersma.domain.TrackList;
import nl.imreboersma.domain.User;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class Spotitube {

  private IUserDAO userDAO = new UserDAO();
  private ITrackListDAO iTrackListDAO = new TrackListDAO();

  @POST
  @Path("login")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response postLogin(@NotNull User user) {
    Token token;
    token = userDAO.login(user.getUser(), user.getPassword());
    if(token == null) {
      return Response.status(401).build();
    }
    return Response.status(200).entity(token).build();
  }

  @GET
  @Path("tracks")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getTracks(@QueryParam("token") String token, @QueryParam("forPlaylist") Integer forPlaylist) {
    TrackList trackList = new TrackList();
    trackList.setTracks(iTrackListDAO.getTracks(token, forPlaylist));
    if(userDAO.getUser(token) == null) {
      return Response.status(403).build();
    }
    return Response.status(200).entity(trackList).build();
  }

  @Inject
  public void setIUserDAO(IUserDAO iUserDAO) {
    this.userDAO = iUserDAO;
  }

  @Inject
  public void setITrackListDAO(ITrackListDAO iTrackListDAO) {
    this.iTrackListDAO = iTrackListDAO;
  }
}
