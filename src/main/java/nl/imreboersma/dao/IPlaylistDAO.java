package nl.imreboersma.dao;

import nl.imreboersma.domain.Playlist;
import nl.imreboersma.domain.PlaylistList;
import nl.imreboersma.domain.Track;
import nl.imreboersma.domain.TrackList;

public interface IPlaylistDAO {
  PlaylistList postPlaylist(Playlist playlist, String token);

  PlaylistList putPlaylist(Playlist playlist, Integer id, String token);

  PlaylistList deletePlaylist(Integer id, String token);

  TrackList getTracksFromPlaylist(Integer id);

  void addTrackToPlaylist(Track track, Integer id, String token);

  void deleteTrackFromPlaylist(int playlistId, int trackId, String token);
}
