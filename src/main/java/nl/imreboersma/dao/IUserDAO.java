package nl.imreboersma.dao;

import nl.imreboersma.domain.Token;

import javax.sql.DataSource;

public interface IUserDAO {
  Token login(String username, String password);

  String getUser(String token);

  void setDataSource(DataSource dataSource);
}
