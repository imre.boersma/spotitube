package nl.imreboersma.dao;

import nl.imreboersma.domain.Playlist;

import java.util.ArrayList;

public interface IPlaylistListDAO {
  /**
   * Returns length of all tracks within all playlists
   *
   * @param playlists List of playlists to calculate length of
   * @return Length as Integer
   */
  Integer calculateLength(ArrayList<Playlist> playlists);

  /**
   * Returns list of all playlists
   *
   * @param token sent by front-end
   * @return List of all playlists from datasource
   */
  ArrayList<Playlist> getPlaylists(String token);
}
