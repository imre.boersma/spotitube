package nl.imreboersma.dao;

import nl.imreboersma.domain.Track;

import java.util.ArrayList;

public interface ITrackListDAO {
  ArrayList<Track> getTracks(String token, int forPlaylist);
}
