package nl.imreboersma.dao;

import nl.imreboersma.domain.Track;

import java.util.ArrayList;

public interface ITrackDAO {
  ArrayList<Track> getTracks(Integer id);
}
