package nl.imreboersma.dao;

import nl.imreboersma.domain.Playlist;
import nl.imreboersma.domain.Track;

import javax.annotation.Resource;
import javax.enterprise.inject.Default;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Default
public class PlaylistListDAO implements IPlaylistListDAO {

  @Resource(name = "jdbc/MySQLDB")
  DataSource dataSource;

  @Override
  public Integer calculateLength(ArrayList<Playlist> playlists) {
    int totalLength = 0;
    for(Playlist playlist : playlists) {
      if(playlist.getTracks() != null) {
        for(Track track : playlist.getTracks()) {
          totalLength += track.getDuration();
        }
      } else {
        return 0;
      }
    }
    return totalLength;
  }

  @Override
  public ArrayList<Playlist> getPlaylists(String token) {
    if(token == null) {
      return null;
    }
    ArrayList<Playlist> playlists = new ArrayList<>();
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT * FROM playlist";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      ResultSet resultSet = preparedStatement.executeQuery();
      while(resultSet.next()) {
        Playlist playlist = new Playlist();

        playlist.setId(resultSet.getInt("id"));
        playlist.setName(resultSet.getString("name"));
        playlist.setOwner(resultSet.getString("owner").equals(token));
        playlists.add(playlist);
      }
    } catch(SQLException e) {
      e.printStackTrace();
      return null;
    }
    return playlists;
  }
}
