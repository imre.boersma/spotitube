package nl.imreboersma.dao;

import nl.imreboersma.domain.Token;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements IUserDAO {
  @Resource(name = "jdbc/MySQLDB")
  DataSource dataSource;

  @Override
  public Token login(String username, String password) {

    Token token = new Token();
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT token, username FROM user WHERE username = ? AND password = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setString(1, username);
      preparedStatement.setString(2, password);
      ResultSet resultSet = preparedStatement.executeQuery();
      int count = 0;
      while(resultSet.next()) {
        token.setToken(resultSet.getString("token"));
        token.setUser(resultSet.getString("username"));
        count++;
      }
      if(count == 0) {
        return null;
      }
    } catch(SQLException e) {
      e.printStackTrace();
      return null;
    }
    return token;
  }

  @Override
  public String getUser(String token) {
    String username = "";
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT username FROM user WHERE token = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setString(1, token);
      ResultSet resultSet = preparedStatement.executeQuery();
      while(resultSet.next()) {
        username = resultSet.getString("username");
      }
    } catch(SQLException e) {
      e.printStackTrace();
    }
    return username;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
}
