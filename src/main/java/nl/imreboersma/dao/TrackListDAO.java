package nl.imreboersma.dao;

import nl.imreboersma.domain.Track;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TrackListDAO implements ITrackListDAO {

  @Resource(name = "jdbc/MySQLDB")
  DataSource dataSource;

  @Override
  public ArrayList<Track> getTracks(String token, int forPlaylist) {
    if(token == null) {
      return null;
    }
    ArrayList<Track> tracks = new ArrayList<>();
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT * FROM track WHERE id NOT IN (SELECT track.id FROM track INNER JOIN track_playlist ON track.id = track_playlist.track WHERE track_playlist.playlist = ?)";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, forPlaylist);
      ResultSet resultSet = preparedStatement.executeQuery();
      TrackDAO.setTrackByType(tracks, resultSet);
    } catch(SQLException e) {
      e.printStackTrace();
      return null;
    }
    return tracks;
  }
}
