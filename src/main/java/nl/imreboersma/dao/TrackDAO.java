package nl.imreboersma.dao;

import nl.imreboersma.domain.Song;
import nl.imreboersma.domain.Track;
import nl.imreboersma.domain.Video;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TrackDAO implements ITrackDAO {

  @Resource(name = "jdbc/MySQLDB")
  DataSource dataSource;

  static void setTrackByType(ArrayList<Track> tracks, ResultSet resultSet) throws SQLException {
    while(resultSet.next()) {
      if(resultSet.getString("type").equals("song")) {
        Song song = new Song();
        song.setPerformer(resultSet.getString("performer"));
        song.setTitle(resultSet.getString("title"));
        song.setUrl(resultSet.getString("url"));
        song.setDuration(resultSet.getInt("duration"));
        song.setId(resultSet.getInt("id"));
        song.setAlbum(resultSet.getString("album"));
        tracks.add(song);
      } else if(resultSet.getString("type").equals("video")) {
        Video video = new Video();
        video.setPerformer(resultSet.getString("performer"));
        video.setTitle(resultSet.getString("title"));
        video.setUrl(resultSet.getString("url"));
        video.setDuration(resultSet.getInt("duration"));
        video.setId(resultSet.getInt("id"));
        video.setDescription(resultSet.getString("description"));
        video.setPublicationDate(resultSet.getDate("publicationDate"));
        tracks.add(video);
      }
    }
  }

  @Override
  public ArrayList<Track> getTracks(Integer id) {
    ArrayList<Track> tracks = new ArrayList<>();
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT * FROM track INNER JOIN track_playlist ON track.id = track_playlist.track WHERE track_playlist.playlist = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, id);
      ResultSet resultSet = preparedStatement.executeQuery();
      while(resultSet.next()) {
        Track track = new Track();
        track.setDuration(resultSet.getInt("duration"));
        track.setOfflineAvailable(resultSet.getBoolean("offlineAvailable"));
        track.setPerformer(resultSet.getString("performer"));
        track.setTitle(resultSet.getString("title"));
        track.setUrl(resultSet.getString("url"));
        tracks.add(track);
      }
    } catch(SQLException e) {
      e.printStackTrace();
      return null;
    }
    return tracks;
  }
}
