package nl.imreboersma.dao;

import nl.imreboersma.domain.Playlist;
import nl.imreboersma.domain.PlaylistList;
import nl.imreboersma.domain.Track;
import nl.imreboersma.domain.TrackList;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PlaylistDAO implements IPlaylistDAO {
  @Resource(name = "jdbc/MySQLDB")
  DataSource dataSource;
  IPlaylistListDAO iPlaylistListDAO;
  ITrackListDAO iTrackListDAO;

  @Inject
  public void setiTrackListDAO(ITrackListDAO iTrackListDAO) {
    this.iTrackListDAO = iTrackListDAO;
  }

  @Inject
  public void setiPlaylistListDAO(IPlaylistListDAO iPlaylistListDAO) {
    this.iPlaylistListDAO = iPlaylistListDAO;
  }

  private PlaylistList fetchPlaylists(String token) {
    PlaylistList playlistList = new PlaylistList();
    playlistList.setPlaylists(iPlaylistListDAO.getPlaylists(token));
    playlistList.setLength(iPlaylistListDAO.calculateLength(iPlaylistListDAO.getPlaylists(token)));
    return playlistList;
  }

  @Override
  public PlaylistList postPlaylist(Playlist playlist, String token) {
    try {
      Connection connection = dataSource.getConnection();
      String sql = "INSERT INTO playlist SET name = ?, owner = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setString(1, playlist.getName());
      preparedStatement.setString(2, token);
      preparedStatement.executeQuery();
    } catch(SQLException e) {
      e.printStackTrace();
    }
    return fetchPlaylists(token);
  }

  @Override
  public PlaylistList deletePlaylist(Integer id, String token) {
    try {
      Connection connection = dataSource.getConnection();
      String sql = "DELETE FROM playlist WHERE id = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, id);
      preparedStatement.executeQuery();
    } catch(SQLException e) {
      e.printStackTrace();
    }
    return fetchPlaylists(token);
  }

  @Override
  public TrackList getTracksFromPlaylist(Integer id) {
    TrackList trackList = new TrackList();
    ArrayList<Track> tracks = new ArrayList<>();
    try {
      Connection connection = dataSource.getConnection();
      String sql = "SELECT * FROM track WHERE id IN (SELECT id FROM track INNER JOIN track_playlist ON track.id = track_playlist.track WHERE track_playlist.playlist = ?)";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, id);
      ResultSet resultSet = preparedStatement.executeQuery();
      TrackDAO.setTrackByType(tracks, resultSet);
    } catch(SQLException e) {
      e.printStackTrace();
    }
    trackList.setTracks(tracks);
    return trackList;
  }

  @Override
  public void addTrackToPlaylist(Track track, Integer id, String token) {
    try {
      Connection connection = dataSource.getConnection();
      String sql = "INSERT INTO track_playlist(track, playlist) VALUES (?, ?)";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, track.getId());
      preparedStatement.setInt(2, id);
      preparedStatement.executeQuery();
    } catch(SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void deleteTrackFromPlaylist(int playlistId, int trackId, String token) {
    try {
      Connection connection = dataSource.getConnection();
      String sql = "DELETE FROM track_playlist WHERE track = ? AND playlist = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, trackId);
      preparedStatement.setInt(2, playlistId);
      preparedStatement.executeQuery();
    } catch(SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public PlaylistList putPlaylist(Playlist playlist, Integer id, String token) {
    try {
      Connection connection = dataSource.getConnection();
      String sql = "UPDATE playlist SET name = ? WHERE id = ?";
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setString(1, playlist.getName());
      preparedStatement.setInt(2, id);
      preparedStatement.executeQuery();
    } catch(SQLException e) {
      e.printStackTrace();
    }
    return fetchPlaylists(token);
  }
}
